#--------------------------------------------------------------------------------------------------
# @file    Makefile
# @author  Hotboards Team
# @version V1.1
# @date    26-November-2017
# @brief   Application makefile to build the current project, this makefile
#          applies only for stm32f0 targets, the makefiles make use of the sub makefiles ll.mk
#		   hal.mk, cmsis.mk and also rtos.mk, it is very important user config the rtos.mk
#		   to work with the corresponding port directory, in the case of stm32f0 the folder should be
#		   ARM_CM0, the heap option will be according of the user application
#--------------------------------------------------------------------------------------------------
# Name for your current project
PROJECT = test

#----F0 target to use------------------------------------------------------------------------------
MCU = stm32f072rb

#----library paths (can be relative or absolute)---------------------------------------------------
LL    = ../ll
HAL   = ../hal
CMSIS = ../cmsis
OS    = ../rtos
HIL   = ../hil
HEL   = ../hel

#----Application files to be compiled--------------------------------------------------------------
# user application files
OBJ  = main.o app_msps.o app_ints.o
# cmsis files
OBJ += startup_stm32f072xb.o system_stm32f0xx.o
# rtos files
OBJ += list.o queue.o tasks.o port.o timers.o heap_1.o
# hil drivers
# hel drivers
# hal drivers
OBJ += stm32f0xx_hal.o stm32f0xx_hal_cortex.o stm32f0xx_hal_rcc.o stm32f0xx_hal_rcc_ex.o stm32f0xx_hal_flash.o
OBJ += stm32f0xx_hal_gpio.o stm32f0xx_hal_uart.o stm32f0xx_hal_uart_ex.o stm32f0xx_hal_wwdg.o stm32f0xx_hal_dma.o
OBJ += stm32f0xx_hal_pwr.o stm32f0xx_hal_rtc.o stm32f0xx_hal_spi.o
# ll drivers

#----Linker file for the current project-----------------------------------------------------------
LINKERFILE = $(CMSIS)/cmsisf0/linkers/stm32f072rb.ld

#----Global application symbols, change to -DDEBUG to enable semihosting---------------------------
SYMBOLS += -DNDEBUG				# debug instrucctions activated 
SYMBOLS += -DSTM32F072xB		# micrcontroller device sub familly 
SYMBOLS += -DUSE_HAL_DRIVER		# use hal drivers
SYMBOLS += -DUSE_FULL_ASSERT	# activate function assert_failed

#----Applications paths to files to be compiled (*.c, *.s)-----------------------------------------
VPATH  = app
VPATH += $(CMSIS)/cmsisf0/startups
VPATH += $(HAL)/half0/sources
VPATH += $(LL)/llf0/sources
VPATH += $(OS)/sources
VPATH += $(OS)/ports/ARM_CM0
VPATH += $(OS)/heap
VPATH += $(HIL)
VPATH += $(HEL)

#----Headers file directories(*.h)-----------------------------------------------------------------
INCLUDES  = -I app
INCLUDES += -I configs
INCLUDES += -I $(CMSIS)/cmsis
INCLUDES += -I $(CMSIS)/cmsisf0/core
INCLUDES += -I $(CMSIS)/cmsisf0/registers
INCLUDES += -I $(HAL)/hal
INCLUDES += -I $(HAL)/half0/includes
INCLUDES += -I $(LL)/ll
INCLUDES += -I $(LL)/llf0/includes
INCLUDES += -I $(OS) 
INCLUDES += -I $(OS)/includes 
INCLUDES += -I $(OS)/ports/ARM_CM0
INCLUDES += -I $(HIL)
INCLUDES += -I $(HEL)

#----Stack and Heap values-------------------------------------------------------------------------
HEAP  = 0x00000800
STACK = 0x00000400


#====Apartir de aqui no modifiques nada a menos que sepas lo que hases. ;)=========================
#----Toolchain ( assembler, compiler, linker, etc.. )----------------------------------------------
TOOLSET = arm-none-eabi
CC = $(TOOLSET)-gcc

#----CPU Target------------------------------------------------------------------------------------
CPU  = -mthumb                  # Using the Thumb Instruction Set
CPU += -mcpu=cortex-m0          # The CPU Variant (m0, m0plus ,m3, m4)
CPU += -mlittle-endian          # Endianess implemented
CPU += -mfloat-abi=soft         # Which floating point ABI to use

#----Compiler flags--------------------------------------------------------------------------------
CCFLAGS  = $(CPU)
CCFLAGS += -Os                        # Compile with Size Optimizations (O0, O1, O2, O3, Os)
CCFLAGS += -g3                        # Debugging information level (g1, g2, g3)
CCFLAGS += -ffunction-sections        # Create a separate function section
CCFLAGS += -fdata-sections            # Create a separate data section
CCFLAGS += -fno-builtin               # Don't recognize built-in functions that do not begin with ‘__builtin_’ as prefix
CCFLAGS += -std=c99                   # Comply with C99
CCFLAGS += -Wall                      # Be anal Enable All Warnings
CCFLAGS += -pedantic                  # Be extra anal More ANSI Checks
CCFLAGS += -Wstrict-prototypes        # Warn if a function is declared or defined without specifying the argument types
CCFLAGS += -fsigned-char              # char is treated as signed
CCFLAGS += -fdiagnostics-color=always # color the output
CCFLAGS += -fno-common                # compiler should place uninitialized global variables in the data section
CCFLAGS += -fomit-frame-pointer       # Don't keep the frame pointer in a register for functions that don't need one
CCFLAGS += -fverbose-asm              # Put extra commentary information in the generated assembly code

# Assembler and linker flags-----------------------------------------------------------------------
ASFLAGS = $(CPU)
LDFLAGS = $(CPU) -Wl,--gc-sections 
LDFLAGS += --specs=nano.specs 
LDFLAGS += --specs=rdimon.specs -lc -lrdimon
LDFLAGS += --specs=nosys.specs -lc -lnosys
LDFLAGS += -Wl,-Map=Output/$(PROJECT).map
LDFLAGS += -Xlinker --defsym=_Min_Heap_Size=$(HEAP)
LDFLAGS += -Xlinker --defsym=_Min_Stack_Size=$(STACK)


#----Build the project-----------------------------------------------------------------------------
all : $(PROJECT)

# Generate binaries, assembler list and display the program size
$(PROJECT) : $(addprefix Output/,$(PROJECT).elf)
	$(TOOLSET)-objcopy -Oihex $< Output/$(PROJECT).hex
	$(TOOLSET)-objdump -S $< > Output/$(PROJECT).lst
	$(TOOLSET)-size --format=berkeley $<

# Link files and generate elf file
Output/$(PROJECT).elf : $(addprefix Output/obj/,$(OBJ))
	$(CC) $(LDFLAGS) -T$(LINKERFILE) -o $@ $^

# Compile C source files
Output/obj/%.o : %.c
	mkdir -p Output/obj
	$(CC) $(CCFLAGS) $(INCLUDES) $(SYMBOLS) -c -o $@ $^

# Compile ASM files
Output/obj/%.o : %.s
	$(TOOLSET)-as $(ASFLAGS) -o $@ $^

clean :
	rm -r Output

flash :
	JLinkGDBServer -if SWD -device $(MCU) -nogui
	#openocd -f interface/stlink-v2-1.cfg -f target/stm32f0x.cfg -c "program Output/$(PROJECT).hex verify reset" -c shutdown

open :
	JLinkGDBServer -if SWD -device $(MCU) -nogui
	#openocd -f interface/stlink-v2-1.cfg -f target/stm32f0x.cfg -c "reset_config srst_only srst_nogate"

debug :
	$(TOOLSET)-gdb Output/$(PROJECT).elf -iex "set auto-load safe-path /" -iex "target remote localhost:2331"
	#$(TOOLSET)-gdb Output/$(PROJECT).elf -iex "set auto-load safe-path /" -iex "target remote localhost:3333"
