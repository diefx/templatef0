/**------------------------------------------------------------------------------------------------
  * @author  Diego Perez
  * @version V1.0.0
  * @date    06-June-2020
  * @brief   Template
-------------------------------------------------------------------------------------------------*/

/* Includes -------------------------------------------------------------------------------------*/
#include "hal.h"
#include "rtos.h"
#include "app_bsp.h"
/* Private typedef ------------------------------------------------------------------------------*/
/* Private define -------------------------------------------------------------------------------*/
/* Private macro --------------------------------------------------------------------------------*/
/* Private variables ----------------------------------------------------------------------------*/
hal_wwdg_handle_t  wwdg_handler;

static rtos_timer_t  wwdg_timer;
static rtos_timer_t  beat_timer;
/* Private function prototypes ------------------------------------------------------------------*/
static void app_setup( void );
static void app_timer_wwdg( rtos_timer_t timer );
static void app_timer_beat( rtos_timer_t timer );


/**------------------------------------------------------------------------------------------------
  * @brief  Aplication entry point
  * ---------------------------------------------------------------------------------------------*/
int main ( void )
{
    /* Call the basic initializations like clock configuration */
    app_setup( );

    /*timer to service small proccess like wwdg and heart beatd*/
    wwdg_timer = rtos_timer_create( "wwdg", 40u, 1u, NULL, app_timer_wwdg );
    beat_timer = rtos_timer_create( "beat", 500u, 1u, NULL, app_timer_beat );  

    /* Place as many task need to registered to run */

    /*start the timers, well the timer will start when the shceduler starts*/
    rtos_timer_start( wwdg_timer, 0u );
    rtos_timer_start( beat_timer, 0u );

    /* Run the freertos scheduler */   
    rtos_kernel_startScheduler( );
    /* Program never reach this point */
    __sys_assert( 0u );
}


/**------------------------------------------------------------------------------------------------
 * @brief   General Initializations, the function will be exceuted before the rtos scheduler 
 *          start to run
 * ----------------------------------------------------------------------------------------------*/
static void app_setup( void ) 
{
    hal_gpio_init_t led_struct;

    hal_init( );

    /*configure the heart beat led*/
    led_struct.mode  = _hal_gpio_mode_output_pp;
    led_struct.pull  = _hal_gpio_pullup;
    led_struct.speed = _hal_gpio_speed_freq_low;
    led_struct.pin   = _app_heartbit_pin;
    hal_gpio_init( _app_heartbit_port, &led_struct );

    /*windowed watch dog configuration*/ 
    wwdg_handler.instance       = WWDG;
    wwdg_handler.init.prescaler = _app_wwdg_preeescaler;
    wwdg_handler.init.window    = _app_wwdg_window_lower;
    wwdg_handler.init.counter   = _app_wwdg_max_count;
    wwdg_handler.init.ewi_mode  = _hal_wwdg_ewi_enable;
    hal_wwdg_init( &wwdg_handler );

    /*configure and enable peripheral interrupts*/
    hal_nvic_setPriority( _hal_wwdg_irq, _app_wwdg_priority, _app_wwdg_subPriority );
    hal_nvic_enableIrq( _hal_wwdg_irq );
}


/**------------------------------------------------------------------------------------------------
 * @brief   wwdg timer restars (pet the dog function), this function is one of the timer callbacks
 * @param   timer timer id (not in use)
 * ----------------------------------------------------------------------------------------------*/
static void app_timer_wwdg( rtos_timer_t timer )
{
    hal_wwdg_refresh( &wwdg_handler );
}


/**------------------------------------------------------------------------------------------------
 * @brief   heart beat timer callback
 * @param   timer timer id (not in use)
 * ----------------------------------------------------------------------------------------------*/
static void app_timer_beat( rtos_timer_t timer )
{
    hal_gpio_togglePin( _app_heartbit_port, _app_heartbit_pin );
}


/**------------------------------------------------------------------------------------------------
 * @brief   This fucntion is called whenever there is no other task running, user can add code
 *          to be excuted in the background or could be a good idea to call a sleep instruction
 * ----------------------------------------------------------------------------------------------*/
void rtos_kernel_idleTask( void )
{

}


/**------------------------------------------------------------------------------------------------
 * @brief   Function called every time a context switch ocurres. The executes from within an ISR 
 *          so must be very short, not use  much stack, and not call any API functions that don’t 
 *          end in “FromISR” or “FROM_ISR”.
 * ----------------------------------------------------------------------------------------------*/
void rtos_kernel_tickHook( void )
{

}


/**------------------------------------------------------------------------------------------------
 * @brief   the kernel will jump here when for any reason an object was trying to be created and 
 *          failed because there wasn't left memory set in configTOTAL_HEAP_SIZE
 * ----------------------------------------------------------------------------------------------*/
void rtos_kernel_mallocFailedHook( void )
{
    __sys_assert( 0u );
}


/**------------------------------------------------------------------------------------------------
 * @brief   the kernel will jump here when for any reason a task is created but is not enough stak
 *          to allocate its internal variables
 *          THIS FUCNTION SHOULD ONLY BE USED DURING DEVELOPMENT STAGE
 * @param   task: task where the stack allocation failed
 * @param   task_name: string id assigned to the task that failed do the stack allocation
 * ----------------------------------------------------------------------------------------------*/
void rtos_kernel_StackOverflowHook( rtos_task_t task, char *task_name )
{
    __sys_assert( 0u );
}


/**------------------------------------------------------------------------------------------------
 * @brief   The callback is called just after a wwdg event occurres and righ before a warn reset
 *          user should place the code to handle the necesary steps to reset in a safety fashion
 *          closing any pending process  
 * @param   hwwdg: pointer to wwdg descriptor structure
 * ----------------------------------------------------------------------------------------------*/
void hal_wwdg_callback_earlyWakeup( hal_wwdg_handle_t *hwwdg )
{
    __sys_assert( 0u );
}


/**------------------------------------------------------------------------------------------------
  * @brief  Reports the name of the source file and the source line number where the assert_param
  *         error has occurred. aplication rah here throuhg macro __sys_assert
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * ----------------------------------------------------------------------------------------------*/
void assert_failed( char * file, int line )
{
    /*disable all interrupts to prevent rtos from running again*/
    __rtos_disable_interrupts( );

    #ifndef NDEBUG
         printf( "Suspected error on file: %s, line %d\r\n", file, line );
    #endif
    /* Infinite loop */
    while( 1 )
    {
        /* MISRA */
    }
}
